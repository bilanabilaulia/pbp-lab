1. Apakah perbedaan antara JSON dan XML?


Pengertian:

a. JSON = disebut juga JavaScript Object Notation yang fungsinya untuk menyimpan dan mengirimkan informasi

b. XML = disebut juga xtensible markup language yang fungsinya untuk menyimpan dan mengirimkan data


Tipe:

a. JSON = Bahasa Meta

b. XML = Bahasa Markup


Komplesitas:

a. JSON = Lebih sederhana dari XML

b. XML = Lebih rumit


Array:

a. JSON = Mendukung array

b. XML = Tidak mendukung array


Referensi : 
https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html

https://www.monitorteknologi.com/perbedaan-json-dan-xml/




2. Apakah perbedaan antara HTML dan XML?


Pengertian:

a. HTML = disebut juga Hypertext Markup Language yang fungsinya lebih kepada penyajian data

b. XML = disebut juga extensible markup language yang fungsinya untuk menyimpan dan mengirimkan data


Urutan:

a. HTML = Tidak terlalu diperhitungkan

b. XML = Urutan diperhatikan dan harus benar


Tipe Bahasa:

a. HTML = Case insensitive

b. XML = Case sensitive


Tujuan:

a. HTML = Fokus pada penyajian data

b. XML = Fokus pada penyimpanan dan pengiriman data.


Referensi:
https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html

https://gitlab.com/PBP-2021/pbp-lab/-/blob/master/lab_instruction/lab_2/README.md 