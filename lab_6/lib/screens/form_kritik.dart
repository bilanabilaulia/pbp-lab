import 'package:flutter/material.dart';
import 'package:lab_6/main.dart';

class FormKritik extends StatefulWidget{
  //final void Function(String?)? onSave;
  //final FormFieldValidator<String>? validator;
  @override
  State<StatefulWidget> createState(){
    return FormKritikState();
  }
}

class FormKritikState extends State<FormKritik>{

  late String _role;
  late String _keluhan;
  late String _pesan;
  late String _rating;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  //final void Fun
  //get onPressed => null;

  List<String> role=["Admin", "Donatur", "Pengaju"];
  String _roleDef="Admin";

  List<String> rate=["1", "2", "3" , "4", "5"];
  String _rateDef="5";

  void pilihRole(String value){
    setState(() {
      _roleDef=value;
    });
  }

  void pilihRate(String value){
    setState(() {
      _rateDef=value;
    });
  }

  Widget _tempatRole(){
    return Row(
      children:<Widget> [
        const Text("Role    ", style: TextStyle(fontSize: 18, color: myColor1),),
        DropdownButton(
          onChanged: (String? value){
            pilihRole(value!);
          },
          value: _roleDef,
          items: role.map((String value){
            return DropdownMenuItem(
              value: value,
              child: Text(value),
            );
          }).toList(),
        )
      ],
    );
  }

  Widget _tempatKeluhan(){
    return TextFormField(
      decoration: InputDecoration(
        hintText: "Keluhan",
        labelText: "Keluhan",
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0)
        ),
      ),
      validator: (String? value){
        if(value!=null&&value.isEmpty){
          return 'Silakan isi bagian ini';
        }
      },
      onSaved: (String? value){
        _keluhan=value!;
      },
    );


  }
  //
  Widget _tempatPesan() {
    return TextFormField(
      maxLines: 4,
      decoration: InputDecoration(
        hintText: "Pesan",
        labelText: "Pesan",
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0)
        ),
      ),
      validator: (String? value) {
        if (value != null && value.isEmpty) {
          return 'Silakan isi bagian ini';
        }
      },
      onSaved: (String? value) {
        _keluhan = value!;
      },
    );
  }
  //
  Widget _tempatRating(){
    return Row(
      children:<Widget> [
        const Text("Rate    ", style: TextStyle(fontSize: 18, color: myColor1),),
        DropdownButton(
          onChanged: (String? value){
            pilihRate(value!);
          },
          value: _rateDef,
          items: rate.map((String value){
            return DropdownMenuItem(
              value: value,
              child: Text(value),
            );
          }).toList(),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title: const Text("HelPINK U")),
      body: Container(
        margin: const EdgeInsets.all(24),
        child: Form(
          key: _formKey,
          child: ListView(
            /*mainAxisAlignment: MainAxisAlignment.center,*/
            children: <Widget>[
              const Text("KRITIK DAN SARAN", style: TextStyle(color: myColor1, fontSize: 30),),
              const Padding(padding: EdgeInsets.only(top: 20.0)),
              _tempatRole(),
              const Padding(padding: EdgeInsets.only(top: 15.0)),
              _tempatKeluhan(),
              const Padding(padding: EdgeInsets.only(top: 15.0)),
              _tempatPesan(),
              const Padding(padding: EdgeInsets.only(top: 15.0)),
              _tempatRating(),
              const SizedBox(height: 50),
              ElevatedButton(
                  onPressed: (){
                    if(!_formKey.currentState!.validate()){
                      return;
                    }
                    _formKey.currentState!.save();

                  },
                  child: const Text('Submit')
              )
            ],
        )),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.mail),
            label: 'Kritik',
            backgroundColor: myColor1,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: 'Business',
            backgroundColor: Colors.green,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: 'School',
            backgroundColor: Colors.purple,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
            backgroundColor: Colors.pink,
          ),
        ],
        // currentIndex: _selectedIndex,
        // selectedItemColor: Colors.amber[800],
        // onTap: _onItemTapped,
      ),
    );
  }
}