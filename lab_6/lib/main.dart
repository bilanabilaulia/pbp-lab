import 'package:flutter/material.dart';
//import 'package:lab_6/screens/drawer.dart';
//import 'package:lab_6/screens/konten.dart';
//import 'package:lab_6/widgets/kritik.dart';
import 'package:lab_6/screens/form_kritik.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HelPINK U',
      theme: ThemeData(
        primarySwatch: myColor1,
      ),
      home: FormKritik(),

      //home: const MyHomePage(title: 'HelPINK U'),
    );
  }
}

const MaterialColor myColor1 =  MaterialColor(
    0xFFC37B89,
  <int, Color>{
    50: Color(0xFFC37B89),
    100: Color(0xFFC37B89),
    200: Color(0xFFC37B89),
    300: Color(0xFFC37B89),
    400: Color(0xFFC37B89),
    500: Color(0xFFC37B89),
    600: Color(0xFFC37B89),
    700: Color(0xFFC37B89),
    800: Color(0xFFC37B89),
    900: Color(0xFFC37B89),
  }
);
//const SecondaryColor =  Color(0xFFF3F4ED);
const MaterialColor myColor2 =  MaterialColor(
    0xFFF3F4ED,
    <int, Color>{
      50: Color(0xFFF3F4ED),
      100: Color(0xFFF3F4ED),
      200: Color(0xFFF3F4ED),
      300: Color(0xFFF3F4ED),
      400: Color(0xFFF3F4ED),
      500: Color(0xFFF3F4ED),
      600: Color(0xFFF3F4ED),
      700: Color(0xFFF3F4ED),
      800: Color(0xFFF3F4ED),
      900: Color(0xFFF3F4ED),
    }
);



// class MyHomePage extends StatefulWidget {
//   const MyHomePage({Key? key, required this.title}) : super(key: key);


//   final String title;

//   @override
//   State<MyHomePage> createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   int _counter = 0;

//   void _incrementCounter() {
//     setState(() {
//       // This call to setState tells the Flutter framework that something has
//       // changed in this State, which causes it to rerun the build method below
//       // so that the display can reflect the updated values. If we changed
//       // _counter without calling setState(), then the build method would not be
//       // called again, and so nothing would appear to happen.
//       _counter++;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     // This method is rerun every time setState is called, for instance as done
//     // by the _incrementCounter method above.
//     //
//     // The Flutter framework has been optimized to make rerunning build methods
//     // fast, so that you can just rebuild anything that needs updating rather
//     // than having to individually change instances of widgets.
//     return Scaffold(
//       appBar: AppBar(
//         // Here we take the value from the MyHomePage object that was created by
//         // the App.build method, and use it to set our appbar title.
//         title: Text(widget.title),
//       ),
//       //drawer: DrawerHelPINKU();
//       body: KontenKritik(),
//       // Center(
//       //   // Center is a layout widget. It takes a single child and positions it
//       //   // in the middle of the parent.
//       //   child: Column(
//       //     // Column is also a layout widget. It takes a list of children and
//       //     // arranges them vertically. By default, it sizes itself to fit its
//       //     // children horizontally, and tries to be as tall as its parent.
//       //     //
//       //     // Invoke "debug painting" (press "p" in the console, choose the
//       //     // "Toggle Debug Paint" action from the Flutter Inspector in Android
//       //     // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
//       //     // to see the wireframe for each widget.
//       //     //
//       //     // Column has various properties to control how it sizes itself and
//       //     // how it positions its children. Here we use mainAxisAlignment to
//       //     // center the children vertically; the main axis here is the vertical
//       //     // axis because Columns are vertical (the cross axis would be
//       //     // horizontal).
//       //     mainAxisAlignment: MainAxisAlignment.center,
//       //     children: <Widget>[
//       //       const Text(
//       //         'You have pushed the button this many times:',
//       //       ),
//       //       Text(
//       //         '$_counter',
//       //         style: Theme.of(context).textTheme.headline4,
//       //       ),
//       //     ],
//       //   ),
//       // ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: _incrementCounter,
//         tooltip: 'Increment',
//         child: const Icon(Icons.add),
//       ), // This trailing comma makes auto-formatting nicer for build methods.
//     );
//   }
// }
