from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.http.response import HttpResponse
from django.core import serializers
from django.contrib.auth.decorators import login_required
# Create your views here.

def index (request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def xml (request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json (request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json") 

#@login_required(login_url = "/admin/login/")
def add_note(request):
    context ={}
    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list (request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
